class Api::V1::CompaniesController < ApplicationController
	before_action :authenticate_user!, only: [:show]

	def index
		if params[:name] && params[:type]
			@companies = Company.where("name LIKE :name AND ctype = :type", {:name => "#{params[:name]}%", :type => params[:type]})
		elsif params[:name]
			@companies = Company.where("name LIKE :name", {:name => "#{params[:name]}%"})
		elsif params[:type]
			@companies = Company.where("ctype = :type", {:type => params[:type]})
		else 
			@companies = Company.all
		end
	end 

	def show
		@company = Company.find(params[:id])
	end
end
